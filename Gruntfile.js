module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
            compile: {
                files: {
                    'compile/css/main.css': 'app/less/**/*.less'
                }
            }
        },
        watch: {
            options: {
                livereload: true
            },

            js: {
                files: 'app/js/**/*.js',
                tasks: ['jshint', 'concat:compile']
            },
            less: {
                files: 'app/less/**/*.less',
                tasks: 'less:compile'
            },
            html: {
                files: 'app/index.html',
                tasks: 'preprocess:compile'
            }
            
        },

        express: {
            all: {
                options: {
                    port: 9000,
                    hostname: 'localhost',
                    bases: 'compile/',
                    livereload: true
                }
            }
        },

        jshint: {
            options: {},
            files: ['app/main.js']
        },

        concat: {
            options: {
                separator: ';',
            },
            compile: {
                src: [
                    'app/js/commons/constans.js',
                    'app/js/todoModel.js',
                    'app/js/todoDomElement.js',
                    'app/js/todoView.js',
                    'app/js/todoCollectionView.js',
                    'app/js/app.js',
                    'app/js/main.js'
                ],
                dest: 'compile/js/main.js',
            },
        },

        clean: {
            // build: ["path/to/dir/one", "path/to/dir/two"],
            compile: 'compile/'
        },

        preprocess: {
              compile: {
                  src: 'app/index.html',
                  dest: 'compile/index.html'
              }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-express');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-preprocess');

    grunt.registerTask('default', [
        'server',
    ]);

    grunt.registerTask('server', [
        'clean:compile',
        'express',
        'preprocess:compile',
        'concat:compile',
        'jshint',
        'less',
        'watch'
    ]);
};
