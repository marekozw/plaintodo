var constans = (function () {
    var constans = Object.create({});

    Object.defineProperty(constans, 'TODO_DEFAULT_TITLE', {
         enumerable: false,
         configurable: false,
         writable: false,
         value: 'Untitled Todo'
     });

    return constans;
})();
