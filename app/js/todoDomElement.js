var TodoDomElement = (function () {

    // === Constructor ===
    var TodoDomElement = function (text) {
        // === Privates
        var _textNode = document.createTextNode(text),
            _span = document.createElement('span'),
            _element = document.createElement('li'),
            _checkBox = document.createElement('input'),
            _textBox = document.createElement('input');

        _checkBox.type = 'checkbox';

        _textBox.type = 'text';
        _textBox.style.visibility = 'hidden';

        _span.appendChild(_textNode);
        
        _element.appendChild(_checkBox);
        _element.appendChild(_span);
        _element.appendChild(_textBox);

        _element.checkBox = _checkBox;
        _element.textBox = _textBox;
        _element.span = _span;

        _element.className = 'todo'

        _element.editMode = function (text) {
            this.textBox.style.visibility = 'visible';
            this.textBox.value = text;

            this.checkBox.style.visibility = 'hidden';
            this.span.style.visibility = 'hidden';

            this.textBox.focus();
            this.textBox.select();
        };

        _element.normalMode = function (text) {
            this.checkBox.style.visibility = 'visible';
            this.span.style.visibility = 'visible';
            this.span.textContent = text;

            this.textBox.style.visibility = 'hidden';
        };

        return _element;
    };

    return TodoDomElement;
})();
