var TodoCollectionView = (function (TodoView) {
    
    // === Constructor ===
    var TodoCollectionView = function () {
        var _todoViews = [],
            _domElement;

        _domElement = document.createElement('ul');

        // === Innerscoped Methods ===
        this.domElement = function () { // only getter
            return _domElement;
        };

        this.todoViews = function () { // onyl getter
            return _todoViews;
        };
    };

    // === Prototype ===
    TodoCollectionView.prototype.addTodo = function (todoModel) {
        var newView = new TodoView(todoModel);

        this.todoViews().push(newView);
        this.domElement().appendChild(newView.domElement());
    };

    TodoCollectionView.prototype.removeTodo = function (todoView) {
        var todoViewIndex = this.todoViews().indexOf(todoView);

        todoView.onDestroy();
        this.todoViews().splice(todoViewIndex, 1);
        this.domElement().removeChild(todoView.domElement());
    };

    TodoCollectionView.prototype.updateTodo = function (todoView, newText) {
        todoView.text(newText);
        todoView.refreshLabel();
    };

    return TodoCollectionView;
})(TodoView);
