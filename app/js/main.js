(function (PlainTodoApp) {
    var bootstrapApplication = function () {
        var application = new PlainTodoApp();
        application.run();
    };

    window.onload = bootstrapApplication;
})(PlainTodoApp);
