var PlainTodoApp = (function (TodoModel, TodoCollectionView) {
    
    var isTextValid = function (text) {
        return text && text.length > 0;
    };

    var isElementUnique = function (text, todoViews) {
        var isUnique = true;

        for (var i = 0, l = todoViews.length; i < l; i++) {
            if (todoViews[i].text() === text) {
                isUnique = false;
                break;
            }
        }

        return isUnique;
    };

    var PlainTodoApp = function () {
        var _newTodoInput = document.getElementById('add-todo-input'),
            _newTodoBtn = document.getElementById('add-todo-btn'),
            _removeToArchiveBtn = document.getElementById('remove-to-archive'),
            _todoListContainer = document.getElementById('list'),
            
            _todoList = new TodoCollectionView();

        var addNewTodoListener = function () {
            var text = _newTodoInput.value;
            if (isTextValid(text) && isElementUnique(text, _todoList.todoViews())) {
                _todoList.addTodo(new TodoModel(text));
                _newTodoInput.value = '';
            }
        };

        var removeToArchiveListener = function () {
            var views = _todoList.todoViews(),
                viewsToRemove = views.filter(function (todoView) {
                    return todoView.isDone();
                });

            viewsToRemove.forEach(function (viewToRemove) {
                _todoList.removeTodo(viewToRemove);
            });
        };

        this.run = function () {
            _todoListContainer.appendChild(_todoList.domElement());

            _newTodoBtn.addEventListener('click', function (event) {
                addNewTodoListener();
            });

            _newTodoInput.addEventListener('keyup', function (event) {
                if (event.keyCode === 13) { // enter
                    addNewTodoListener();
                }
            });

            _removeToArchiveBtn.addEventListener('click', function (eveny) {
                removeToArchiveListener();
            });

            document.addEventListener('edit-todo', function (event) {
                var newText = event.newText,
                    todoView = event.todoView;

                if (isTextValid(newText) && isElementUnique(newText, _todoList.todoViews())) {
                    _todoList.updateTodo(todoView, newText);
                }
            });
        };

    };

    return PlainTodoApp;
})(TodoModel, TodoCollectionView);
