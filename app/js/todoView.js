var TodoView = (function (TodoDomElement) {
   
    // === Constructor ===   
    var TodoView = function (todoModel) {
        // === Privates ===
        var _todoModel,
            _domElement,
            _that = this;

        var refreshLabel = function () {
            if (_todoModel.isDone()) {
                _domElement.className += ' complete';
            } else {
                _domElement.classList.remove('complete');
            }
        };

        var refreshStructure = function () {
            if (_todoModel.isEditing()) {
                _domElement.editMode(_todoModel.text());
            } else {
                _domElement.normalMode(_todoModel.text());
            }
        };

        _todoModel = todoModel;
        _domElement = new TodoDomElement(todoModel.text());

        // === DOM event handlers ===
        function onCheckBoxChange (event) {
            if (event.target.checked) {
                _todoModel.setComplete();
            } else {
                _todoModel.setUncomplete();
            }

            refreshLabel();
        }

        function onSpanClick () {
            if (_todoModel.isDone()) {
                return;
            }

            _todoModel.startEditing();
            refreshStructure();
        }

        function onTextBoxKeyup (event) {
            var editTodoEvent;

            if (event.keyCode === 13) { // enter
                editTodoEvent = new Event('edit-todo');
                editTodoEvent.newText = event.target.value;
                editTodoEvent.todoView = _that;
                document.dispatchEvent(editTodoEvent);

                _todoModel.stopEditing();
                refreshStructure();
            } else if (event.keyCode === 27) { // escape
                _todoModel.stopEditing();
                refreshStructure();
            }
        }

        _domElement.checkBox.addEventListener('change', onCheckBoxChange);
        _domElement.span.addEventListener('click', onSpanClick);
        _domElement.textBox.addEventListener('keyup', onTextBoxKeyup);

        // === Innerscoped Methods ===
        this.domElement = function () { // only getter
            return _domElement;
        };

        this.isDone = function () { // only getter
            return _todoModel.isDone();
        };

        this.text = function (text) {    
            return _todoModel.text(text);
        };

        this.onDestroy = function () { // remove listeners before destroying
            _domElement.checkBox.removeEventListener('change', onCheckBoxChange);
            _domElement.span.removeEventListener('click', onSpanClick);
            _domElement.textBox.removeEventListener('keyup', onTextBoxKeyup);
        };

        this.refreshLabel = refreshLabel;
        this.refreshStructure = refreshStructure;
    };

   return TodoView;
})(TodoDomElement);
