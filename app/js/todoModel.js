var TodoModel = (function (constans) {

    var isTextValid = function (text) {
        return text && text.length > 0;
    };

    // === Constructor ===
    var TodoModel = function (text) {
        // === Private ===
        var _text = constans.TODO_DEFAULT_TITLE,
            _isDone = false;
            _isEditing = false;
        
        if (isTextValid(text)) {
            _text = text;
        }

        // === Innerscoped Methods ===
        this.text = function (text) {
            if (text !== undefined && isTextValid(text)) {
                _text = text;
                return this;
            }
            return _text;
        };

        this.isDone = function (isDone) {
            if (isDone !== undefined) {
                _isDone = Boolean(isDone);
                return this;
            }
            return _isDone;
        };

        this.isEditing = function (isEditing) {
            if (isEditing !== undefined) {
                _isEditing = Boolean(isEditing);
                return this;
            }
            return _isEditing;
        };
    };

    // === Prototype ===
    TodoModel.prototype.setComplete = function () {
        this.isDone(true);
        return this;
    };
    
    TodoModel.prototype.setUncomplete = function () {
        this.isDone(false);
        return this;
    };

    TodoModel.prototype.startEditing = function () {
        this.isEditing(true);
        return this;
    };
    TodoModel.prototype.stopEditing = function () {
        this.isEditing(false);
        return this;
    };

    return TodoModel;
})(constans);
